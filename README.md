# Ordinal-Time Tool #

Quick tool that reads in a geoJSON or kml file and puts all entries onto a timeline map in the order that they appear in the file (top to bottom)