/**************************/
/*      CONSTS & VARS     */
/**************************/
const uploadFile = document.getElementById('uploadFile');
const loadBtn = document.getElementById('load-button');

const places = [
    {
        name: "some place 1",
        startDate: "11-JUN-2020",
        geo: {latitude: -32.8945, longitude: 151.6976}
    },
    {
        name: "some place 2",
        startDate: "12-JUN-2020",
        geo: {latitude: -33.8945, longitude: 151.6976}
    },
    {
        name: "some place 3",
        startDate: "13-JUN-2020",
        geo: {latitude: -34.8945, longitude: 151.6976}
    }
]

/**************************/
/*         EVENTS         */
/**************************/
/* Load json/kml button clicked */
$(loadBtn).on('click', function() {
    uploadFile.click(); //fires $(uploadFile).change event after file select
})

/* File Selected */
$(uploadFile).change(function() {
    var fileToLoad = uploadFile.files[0];
    importFile(fileToLoad)
})

/**************************/
/*       FUNCTIONS        */
/**************************/
function importFile(file) {
    if (file) {
        var ext = file.name.split('.')[1] //file extension
        if (ext == 'json' || ext == 'geojson') { geoJSONReader(file); $(loadBtn).hide() }
        else if (ext == 'kml') { kmlReader(file); $(loadBtn).hide() }
        else {
            error()
            return
        }
    }
}

function geoJSONReader(file) {
    var reader = new FileReader();
    reader.readAsText(file, "UTF-8")
    reader.onload = function (e) {
        //...
        loadPlaces(places)
    }
}

function kmlReader(file) {
    var reader = new FileReader();
    reader.readAsText(file, "UTF-8")
    reader.onload = function (e) {
        //...
        loadPlaces(places)
    }
}

function error() {
    alert('Error loading file. Is it the correct file type (.kml .json or .geojson)? Is it valid kml/geojson?')
}